# ParaView Superbuild options
set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")
set(USE_NONFREE_COMPONENTS          ON CACHE BOOL "")

# ParaView Superbuild projects
set(ENABLE_cosmotools               ON CACHE BOOL "")
set(ENABLE_egl                      OFF CACHE BOOL "")
set(ENABLE_ffmpeg                   ON CACHE BOOL "")
set(ENABLE_fortran                  ON CACHE BOOL "")
set(ENABLE_gdal                     ON CACHE BOOL "")
set(ENABLE_gmsh                     ON CACHE BOOL "")
set(ENABLE_launchers                ON CACHE BOOL "")
set(ENABLE_matplotlib               ON CACHE BOOL "")
set(ENABLE_mesa                     ON CACHE BOOL "")
set(ENABLE_mili                     ON CACHE BOOL "")
set(ENABLE_mpi                      ON CACHE BOOL "")
set(ENABLE_netcdf                   ON CACHE BOOL "")
set(ENABLE_nlohmannjson             ON CACHE BOOL "")
set(ENABLE_numpy                    ON CACHE BOOL "")
set(ENABLE_nvidiaindex              ON CACHE BOOL "")
set(ENABLE_openpmd                  ON CACHE BOOL "")
set(ENABLE_osmesa                   OFF CACHE BOOL "")
set(ENABLE_paraview                 ON CACHE BOOL "")
set(ENABLE_paraviewgettingstartedguide  ON CACHE BOOL "")
set(ENABLE_paraviewpluginsexternal  ON CACHE BOOL "")
set(ENABLE_paraviewsdk              OFF CACHE BOOL "")
set(ENABLE_paraviewtutorialdata     ON CACHE BOOL "")
set(ENABLE_paraviewweb              ON CACHE BOOL "")
set(ENABLE_python                   ON CACHE BOOL "")
set(ENABLE_python3                  ON CACHE BOOL "")
set(ENABLE_pythonpandas             ON CACHE BOOL "")
set(ENABLE_pythonpygments           ON CACHE BOOL "")
set(ENABLE_qt5                      ON CACHE BOOL "")
set(ENABLE_scipy                    ON CACHE BOOL "")
set(ENABLE_silo                     ON CACHE BOOL "")
set(ENABLE_szip                     ON CACHE BOOL "")
set(ENABLE_tbb                      ON CACHE BOOL "")
set(ENABLE_visitbridge              ON CACHE BOOL "")
set(ENABLE_vrpn                     ON CACHE BOOL "")
set(ENABLE_vtkm                     ON CACHE BOOL "")
set(ENABLE_xdmf3                    ON CACHE BOOL "")
set(ENABLE_zfp                      ON CACHE BOOL "")

# Qt5 related
set(PACKAGE_SYSTEM_QT ON CACHE BOOL "")

# ParaView related
set(paraview_SOURCE_SELECTION "source" CACHE STRING "")

# MPI related, to disable when building for the release without MPI
set(ENABLE_mpi ON CACHE BOOL "")

# Projects we choose to disable because they require access to Kitware internal network
# set(ENABLE_nvidiaindex ON CACHE BOOL "")
# set(ENABLE_visrtx      ON CACHE BOOL "")
# set(ENABLE_nvidiaoptix ON CACHE BOOL "")

# Projects that are known to fails under specifics conditions
# Enable only if needed for your plugin
# set(ENABLE_lookingglass     ON CACHE BOOL "")
# set(ENABLE_vortexfinder2    ON CACHE BOOL "")
# set(ENABLE_adios2           ON CACHE BOOL "")
# set(ENABLE_openimagedenoise ON CACHE BOOL "")
# set(ENABLE_ospraymaterials  ON CACHE BOOL "")
# set(ENABLE_ospray           ON CACHE BOOL "")
# set(ENABLE_ospraymodulempi  ON CACHE BOOL "")

# Projects that are not needed and may cause issues when configuring plugins
# set(ENABLE_openvr ON CACHE BOOL "") # https://gitlab.kitware.com/paraview/paraview-superbuild/-/issues/131
