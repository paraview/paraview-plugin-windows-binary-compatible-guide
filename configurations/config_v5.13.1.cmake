#####################################################################
## configure_windows_vs2019_mpi.cmake                             ##
#####################################################################

set(ENABLE_mpi ON CACHE BOOL "") 
set(ENABLE_pythonmpi4py ON CACHE BOOL "") 
# https://github.com/ospray/module_mpi/issues/2
set(ENABLE_ospraymodulempi OFF CACHE BOOL "") 

#####################################################################
## configure_windows.cmake                                         ##
#####################################################################

set(PACKAGE_SYSTEM_QT ON CACHE BOOL "")

set(ENABLE_catalyst ON CACHE BOOL "")
## set(ENABLE_lookingglass ON  CACHE BOOL "") CAUSES ISSUES WHEN CONFIGURING PLUGINS
## set(ENABLE_nvidiaindex ON CACHE BOOL "" CANNOT BE ENABLED OUTSIDE OF KITWARE NETWORK
set(ENABLE_openmp OFF CACHE BOOL "")
set(ENABLE_openimagedenoise OFF CACHE BOOL "")
set(ENABLE_openvr ON CACHE BOOL "")
## set(ENABLE_openxrremoting ON CACHE BOOL "") CAUSES ISSUES WHEN CONFIGURING PLUGINS
set(ENABLE_openxrsdk ON CACHE BOOL "")
set(ENABLE_paraviewtranslations ON CACHE BOOL "")
## set(ENABLE_threedxwaresdk ON CACHE BOOL "")  CANNOT BE ENABLED OUTSIDE OF KITWARE NETWORK
set(ENABLE_zeromq ON CACHE BOOL "")
## set(ENABLE_visrtx ON CACHE BOOL "")  CANNOT BE ENABLED OUTSIDE OF KITWARE NETWORK
set(ENABLE_vortexfinder2 OFF CACHE BOOL "")
## set(ENABLE_nvidiaoptix ON CACHE BOOL "")  CANNOT BE ENABLED OUTSIDE OF KITWARE NETWORK
set(ENABLE_zspace ON CACHE BOOL "")

set(qt5_ENABLE_WEBENGINE "OFF" CACHE STRING "")

#####################################################################
## configure_common.cmake                                          ##
#####################################################################

set(USE_NONFREE_COMPONENTS          ON CACHE BOOL "")
set(BUILD_TESTING                   ON CACHE BOOL "")
set(GENERATE_SPDX                   ON CACHE BOOL "")

# The new Mesa has issues with back screens when rendering. See
# paraview/paraview#22152.
set(mesa_SOURCE_SELECTION "21.2.1" CACHE STRING "")

# Build LLVM as static to reduce the number of shared libraries needed.
set(llvm_BUILD_SHARED_LIBS "OFF" CACHE STRING "")

function (enable_project name)
  set("ENABLE_${name}" ON CACHE BOOL "")
endfunction ()

enable_project(adios2)
enable_project(alembic)
enable_project(blosc)
enable_project(blosc2)
enable_project(cinemaexport)
enable_project(cosmotools)
enable_project(exodus) # requires seacas to actually build anything
enable_project(ffmpeg)
enable_project(fortran)
enable_project(gdal)
enable_project(gmsh)
enable_project(h5py)
enable_project(launchers)
enable_project(lz4)
enable_project(matplotlib)
enable_project(medreader)
enable_project(mesa)
enable_project(mili)
enable_project(mpi)
enable_project(netcdf)
enable_project(nlohmannjson)
enable_project(numpy)
## enable_project(nvidiaindex)  CANNOT BE ENABLED OUTSIDE OF KITWARE NETWORK
enable_project(occt)
enable_project(openimagedenoise)
enable_project(openmp)
enable_project(openpmd)
enable_project(openvdb)
enable_project(ospray)
enable_project(ospraymaterials)
enable_project(ospraymodulempi)
enable_project(paraview)
enable_project(paraviewgettingstartedguide)
enable_project(paraviewpluginsexternal)
enable_project(paraviewtutorialdata)
enable_project(paraviewweb)
enable_project(pdal)
enable_project(python3)
enable_project(pythonmpi4py)
enable_project(pythonnetcdf4)
enable_project(pythonpandas)
enable_project(pythonpygments)
enable_project(qt5)
enable_project(scipy)
enable_project(seacas) # needed to actually build exodus
enable_project(silo)
enable_project(surfacetrackercut)
enable_project(sympy)
enable_project(szip)
enable_project(tbb)
enable_project(ttk)
enable_project(visitbridge)
enable_project(vortexfinder2)
enable_project(vrpn)
enable_project(vtkm)
enable_project(xdmf3)
enable_project(xerces)
enable_project(zfp)
enable_project(zstd)

set(ENABLE_egl                      OFF CACHE BOOL "")
set(ENABLE_osmesa                   OFF CACHE BOOL "")
set(ENABLE_paraviewsdk              OFF CACHE BOOL "")
# needs cuda
set(ENABLE_visrtx                   OFF CACHE BOOL "")

# qt5 things
set(qt5_SOURCE_SELECTION            "5.15" CACHE STRING "")

#####################################################################
## configure_cache.cmake                                           ##
#####################################################################

set(qt5_SKIP_PCH "ON" CACHE BOOL "")

#####################################################################
## additions                                                       ##
#####################################################################

set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")
set(paraview_SOURCE_SELECTION "source" CACHE STRING "")
