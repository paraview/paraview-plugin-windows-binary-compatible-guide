# A guide to build ParaView plugins compatible with the Windows binary release of ParaView

This guide is intended to provide a reliable way to build paraview plugins
compatible with the Windows binary release of ParaView, using the ParaView-superbuild
in the same way the binary release of ParaView is built on Windows by Kitware continuous integration.

Using this guide, you will be able to build the ParaView Superbuild with specific
cmake configuration files, and then build your plugins against this
build so that the produced DLL files will load correctly in the actual binary release of ParaView for Windows.

## Install Dependencies
 * Download and install [git bash for windows][gitforwindows].
 * Download and install [cmake][cmake-download] >= 3.22.6.
 * Download and install [Visual Studio 2019 Community Edition][visual-studio], which include the Visual Studio x64 Native Command. Please note newer version may not work.
 * Download [ninja-build][ninja], extract and add its location to the PATH environment variable, or simply drop `ninja.exe` in `C:\Windows\`.
 * Download and install [Qt 5.15][qt-download] for Windows (tested with 5.15.2), make sure to check the corresponding MSVC component during installation.
    * Make sure to add `C:\Qt\[version]\\bin` to your `PATH` environment variable.
    * You may also need to add an environment variable `QT_QPA_PLATFORM_PLUGIN_PATH`: `C:\Qt\Qt[version]\[version]\[MSVC version]\plugins\platforms`.

## About ParaView Version

This guide is intended to work with any version of ParaView >= v5.9.0.
This version is refered as the $RELEASE_TAG in below paragraphs, and sometimes
refered as $RELEASE_VERSION without the leading `v`, eg: 5.9.0.

In the `configurations` folder, you will find CMake configuration file for each version of ParaView
that have been tested with this guide. The path to this file is refered as $CONFIG_FILE_FOR_RELEASE_TAG
in below paragraphs.

These files have been generated from the CI configuration of https://gitlab.kitware.com/paraview/paraview-superbuild.

## About MPI

This guide target the MPI binary release. If for some reason, you want to target the non-MPI binary release,
it is just needed to not enable the MPI project in the CMake configuration file you use when configuring the ParaView superbuild.

## About Qt version

The ParaView Superbuild does not support to build Qt for you on Windows, so you need to install it
as stated in the Dependencies section. All versions of ParaView supported by this guide are using
Qt 5.15.X, which should all be compatible.

Also, if you have multiple Qt installations you may need to set the `Qt5_DIR` CMake variable manually 
during the ParaView Superbuild configuration and Plugin configuration, eg: `-DQt5_DIR=C:/Qt/5.15.2/msvc2019_64/lib/cmake/Qt5`.

## Build the ParaView Superbuild:

Do only once per release you want to target.

* Open git bash (from Windows Start Menu)

```bash
cd C:
mkdir pv
cd pv
git clone https://gitlab.kitware.com/paraview/paraview-superbuild.git -b $RELEASE_TAG --depth=1 --recurse-submodules pvsb
git clone https://gitlab.kitware.com/paraview/paraview -b $RELEASE_TAG --depth=1 --recurse-submodules pv
mkdir build
```

* Expected folder hierarchy

```
C:\pv
    +-pvsb
    +-pv
    +-build
```

Note: If you change folder names and paths, make sure to keep them small to avoid hitting the Windows path length limit of 250 char.

* Open Visual Studio x64 Native Command (from Windows Start Menu)

```bash
cd C:\pv\build
cmake -G Ninja -C $CONFIG_FILE_FOR_RELEASE_TAG -Dparaview_SOURCE_DIR=C:\pv\pv ..\pvsb
ninja
```

Note: The superbuild will download many software source during this step, network connection is needed.

Note: This will take a while, you can `Ctrl-C` and run `ninja` again without losing much progress.

## Build a plugin for the binary release

Do everytime you want to build a plugin.

* Open Visual Studio x64 Native Command (from Windows Start Menu)

```bash
cd C:\path\to\your\plugin
mkdir build
cd build
cmake -G Ninja -DCMAKE_PREFIX_PATH=C:\pv\build\install\ -DCMAKE_BUILD_TYPE=Release ..\
ninja
```

* Recover the dll(s) from build\bin\paraview-$RELEASE_VERSION\plugins\yourPlugin\*.dll
* The main plugin dll can be loaded in the correct binary release of ParaView for Windows and can be shared with anyone using it.
* Your plugin may consists of multiple DLL files, you can use `FORCE_STATIC` in your vtk modules to produce a single DLL file plugin.

## Notes about the process

### Changing a superbuild option after the build
Once the build is complete, enabling/disabling a superbuild cmake option may have no effect.
You should either remove the superbuild/paraview directory and run ninja again or manually
change the configuration as explained below.

### Changing a paraview option after the build
It is perfectly supported to navigate into the ParaView project within
the superbuild build directory (`build\superbuild\paraview\build`) , change the source or reconfigure it, and then build and install it, `cmake ./ && ninja install`.

### Targeting a nightly release
It is possible to target a nightly release of ParaView. Simply checkout the corresponding release tag for the ParaView superbuild and
the correct nightly hash for ParaView, in build ParaView superbuild part. You may need to clone ParaView again for this.

### Miscellaneous
If compiling a plugin contained in the official plugins
of ParaView, do not forget to repackage it like it is shown in the example
plugins, with a supplementary CMakeLists.txt.

When running cmake, cmake-gui or ninja, always use the
Visual Studio x64 Native command.

On Windows, when CMake looks for a lib, point it to a
.lib file, not a .dll file

## Troubleshooting

### Failing to configure a plugin
Depending of the version of ParaView you are trying to build, your plugin may fail to configure
because of missing includes or libs.

If you encounter other error like this, you can easily fix it yourself by:
 * Run cmake-gui pointing to your plugin build
 * Run cmake-gui pointing to superbuild/paraview/build
 * Configure your plugin, note the failing cmake var
 * In the paraview cmake-gui, look for the var and copy the value
 * Paste in into the var of your cmake-gui plugin
 * Repeat until configure is successful

### Failing to load a plugin
If your plugin fails to load in the binary release, check the following:
 * Your plugin is built in Release, not Debug
 * You are using the same version of Qt as the binary release you are targeting
 * You are using/not using the mpi binary release and enabling/disabling MPI in the ParaView superbuild
 * If you are loading on machine without visual studio installed, you may need to install [Microsoft Visual C++ Redistributable][visual-studio-redistributable]

### License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.

[cmake-download]: https://cmake.org/download
[gitforwindows]: https://gitforwindows.org/
[ninja]: https://github.com/ninja-build/ninja/releases
[qt-download]: https://download.qt.io/official_releases/qt/5.15/
[visual-studio]: https://visualstudio.microsoft.com/vs/older-downloads
[visual-studio-redistributable]: https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist
